# nightwing

`nightwing` is a NixOS module that makes it trivial to deploy a secure WiFi mesh networks. More specifically, it sets up secured [802.11s](https://en.wikipedia.org/wiki/IEEE_802.11s) WiFi meshes using [BATMAN V](https://www.open-mesh.org/projects/batman-adv/wiki/Wiki) as their routing algorithm.

Optionally, it can be configured to allow devices on the mesh network to [share their internet connections with other devices on the mesh](#internet connection sharing).

As they say, [a NixOS configuration snippet is worth a thousand words -- check the quickstart guide.](#quickstart)

# What's the point?

I built `nightwing` to use at DEF CON.

Networking at DEF CON is a hot mess. Bandwidth is scarce and there are too many clowns who deliberately attack WiFi networks. Having to wait on package downloads at 15kB/s wastes precious time.

A well-secured mesh network can mostly fix this, assuming there you have enough trusted NixOS friends nearby:

* It provides high bandwidth, low latency connectivity to services running on your friends' laptops (_including `nix-serve`_, effectively enabling peer-to-peer package downloads)

* It's highly mobile: it'll work anywhere in the conference center without manual reconfiguration (so long as you're within range of at least one peer)

* It's cheap: you can realistically obtain compatible WiFi hardware for $30/device.

The only downside is that a mesh network would be a pain to configure, unless someone happened to have written [a handy NixOS module](https://gitlab.com/andrewgrantbrooks/nightwing) to handle that for you.

# Security

<details>
<summary>(click to expand)</summary>

### "Secure" from who?

WiFi networks at DEF CON mainly have two types of adversaries to contend with:

1. Unscrupulous morons who just bought an ESP32 peripheral for their flipper and want to inconvenience others instead of doing something cool

2. _Sophisticated_ morons who want to disrupt any secured WiFi networks to push you onto open networks, which they're presumably monitoring or tampering with

### What are we doing about it?

Meshes created by `nightwing` are more resistant to these adversaries than your run-of-the-mill WiFi network:

*  WPA3-SAE is used to authenticate mesh peers and encrypt traffic.

* Management frames are 802.11w-protected. This blocks common `aireplay-ng` attacks (eg, the deauth attack won't do a damn thing)

* Using BATMAN V for routing allows traffic to be responsively rerouted on the off chance that an adversary does manage to disconnect a device from the mesh.

* The WiFi interfaces are brought up in `mesh-point` mode. While not a proper security measure, it puts the network out of reach of unsophisticated attackers: many WiFi devices or drivers don't support mesh-point. At least one common SkiddieShit™ ESP32 payload can't even see mesh-point stations!

### Things `nightwing` isn't secure from

Look, it's just souped up WiFi with some especially useful settings. `nightwing` makes no attempt to protect you from any of the following:

* [Mossad](https://www.usenix.org/system/files/1401_08-12_mickens.pdf)

* Plain old RF jamming

* Mischief at OSI layers >= 3

* Malicious behavior from other authenticated mesh users (keep that password secret)

</details>

# Quickstart

1. Pick 1+ dedicated WiFi dongles, an IP for each host on mesh, and a WiFi network name/password.

2. Import this module in your `configuration.nix`, eg:
    ```nix
    imports = [ 
      (builtins.getFlake "gitlab:andrewgrantbrooks/nightwing").nixosModules.default
    ];    
    ```

3. Describe and enable at least one BATMAN mesh interface in your `configuration.nix`, eg:
    ```nix
    networking.nightwing = {
      enable = true;
      country = "US"; # so we know which channels can legally be used
      # New network device interface, we're calling it bat0
      networks.bat0 = {
        addressAndMask = "10.69.0.1/24";
        # 1+ dedicated WiFi devices for BATMAN to use, we'll set up one on a 5GHz channel and one
        # on a 2.4 GHz channel as a backup link
        devices = {
          wlo1 = {
            channel = 5240;
            meshID = "MyCool5GHzMeshWiFiName";
            password = "yoursecretpassword";
          };
          wlp0s20f0u1 = {
            channel = 12;
            meshID = "MyCool2.4GHzMeshWiFiName";
            password = "yoursecretpassword";
          };
        };
      };
    };
    ```

# Detailed Instructions

## 1. Get a bucket of _compatible_ WiFi dongles

<details>
<summary>...</summary>

You need a device (and driver for it!) that supports 802.11s.

MediaTek mt76x2u-based devices are a safe bet, cheap, and perform well. The Panda Wireless PAU0D is one such device. If in doubt, go buy one of those.

Commonplace Intel WiFi chips probably won't cut it. I've tested an AX201 unsuccessfully.
</details>

## 2. Plan your network

<details>
<summary>...</summary>

Decide on a network size, pick a static IP for each host participating in the mesh network, decide on a channel for your devices to use, pick a wireless password, etc.

Here are a few tips:

* `iw phy` can tell you which channels are supported by your device(s)

* Don't expect to be able to see your mesh network's ESSID on your phone or when scanning for WiFi networks in NetworkManager

</details>

## 3. Update your NixOS config

<details>
<summary>...</summary>

First, import this module in your system configuration. Assuming you've got flakes turned on, you can add it to your `configuration.nix`'s `imports`, eg:

```nix
imports = [ 
  (builtins.getFlake "gitlab:andrewgrantbrooks/nightwing").nixosModules.default
];
```

Then, tell the module about your intended network/hardware, following the example in this README's [quickstart](#quickstart) section.

You get to choose the name of the BATMAN interface, but you have to use backing WiFi interface names that match those that your system assigns. An easy way to find those is to plug in the dongle and check `dmesg`.
</details>

# Internet connection sharing

<details>
<summary>...</summary>

## Configuration

* Set `networking.nightwing.exitDevice` to the name of an internet-connected network device to share that device's internet connection with other mesh clients.

* Set `networking.nightwing.networks.${name}.routeToExits = true;` on any devices that should try to access the internet through the mesh.

* Set neither if you want neither.

Note that you can have more than one device sharing its internet connection on the mesh. BATMAN will decide which to route to automatically.

## What does that do?

On any hosts sharing their connection, this module sets up NAT between your internet-connected network device and any mesh network(s), including proxy ARP / IP forwarding setup for the relevant interfaces.

On any hosts looking for an internet connection through the mesh, this module adds a low-priority (`metric = 666`) route through your batman device(s) for internet access. The high metric is intended to limit mesh congestion by ensuring that clients typically prefer to use other routes for internet access.

Any actual routing and gateway selection within the mesh is handled by  `BATMAN`'s [gateway mode support](https://www.open-mesh.org/projects/batman-adv/wiki/Gateways).

</details>

# Troubleshooting

## Check hardware compatibility

<details>
<summary>...</summary>

Not all wireless cards are able to operate in mesh-point mode. Even if `iw phy` indicates that your WiFi device can decode mesh-point frames, it is no guarantee that the device can be brought up in mesh-point mode.

If you suspect a hardware support issue, it will likely show up in the logs of the device-specific supplicant service (see below).
</details>

## Check on networkd

<details>
<summary>...</summary>

systemd-networkd brings up/down the BATMAN interface(s), automatically adds/removes wifi interfaces to them, assigns IP addresses, and does basically everything that's not 802.11-specific. 

Use `networkctl` to check on networkd, eg,

```shell
$ sudo networkctl list
IDX LINK        TYPE     OPERATIONAL SETUP     
  1 lo          loopback carrier     unmanaged
  2 bat0        ether    routable    configured
  3 wlo1        wlan     routable    unmanaged
  4 docker0     bridge   no-carrier  unmanaged
 10 wlp0s20f0u1 wlan     enslaved    configured

5 links listed.
```

(in this example, `wlp0s20f0u1` is the dedicated WiFi dongle used by the `bat0` mesh device, so the odd "enslaved" status is expected.)

</details>

## Check on supplicant

<details>
<summary>...</summary>

`wpa_supplicant` manages all the 802.11 setup (think things like setting WiFi password or channel). Its logs describe any association attempts, auth successes/failures, issues changing the channel, etc:

```shell
$ journalctl -xeu supplicant-${your_wifi_dev}.service --since '5 minutes ago'
```
</details>

## Check on BATMAN

<details>
<summary>...</summary>

For any questions specific to BATMAN's internal state / configuration, use `batctl`. 

For example, to see which stations are neighbors in the mesh,

```shell 
$ batctl neighbors
[B.A.T.M.A.N. adv 2023.3, MainIF/MAC: wlp0s20f0u1/9c:ef:d5:f8:91:fd (bat0/ae:79:d9:31:b1:be BATMAN_V)]
IF             Neighbor              last-seen
9c:ef:d5:f8:91:79    0.104s (       89.8) [wlp0s20f0u1]
```
</details>

## Disable (or reconfigure) AQM

<details>
<summary>...</summary>

By default, `nightwing` configures [`tc-cake`](https://www.man7.org/linux/man-pages/man8/tc-cake.8.html) on all BATMAN mesh interfaces that it describes to `systemd-networkd`.

This should generally be safe, but if you want to rule out the qdisc as a source of trouble (or do something fancier?) you can disable this by setting `networking.nightwing.networks.<name>.aqm.enabled` to `false`.

You may also see some benefit from setting `networking.nightwing.networks.<name>.aqm.{roundTripMillisec, bandwidthMbps}` for your network.

</details>
