{
  description = "Instant BATMAN V mesh (just add channel)";
  inputs = {};

  outputs = { self }: {
    # At the time of writing, nixpkgs' wpa_supplicant is not built with
    # support for 802.11s.
    # There's a PR to fix that (https://github.com/NixOS/nixpkgs/pull/291049),
    # but here's an overlay to get older nixpkgs up to speed
    overlays.default = final: prev: {
      wpa_supplicant = with prev;
        if lib.strings.hasInfix "CONFIG_MESH=y" wpa_supplicant.extraConfig
        then wpa_supplicant
        else wpa_supplicant.overrideAttrs (old: {
          preBuild = old.preBuild + ''
            echo "CONFIG_MESH=y" >> .config
          '';
        });
    };

    nixosModules.default = { config, lib, pkgs, ... }:
      let inherit (lib) types attrNames attrValues;
          inherit (types) attrsOf submodule;
          cfg = config.networking.nightwing;
          positiveInt = types.int // {
            check = x: lib.assertMsg (types.int.check x && x > 0)
              ''
              Must be an int > 0
              '';
          };
      in {
      options = {
        networking.nightwing.enable = lib.mkOption {
          type = types.bool;
          default = false;
          description = lib.mdDoc ''
          Set to true to enable all networks defined under `networking.nightwing.networks`.
          '';
        };
        networking.nightwing.country = lib.mkOption {
          type = types.str;
          default = "US";
          description = lib.mdDoc ''
          Regulatory domain to use. This determines which channels are usable by mesh devices.
          Defaults to `US`.

          Take care to set this appropriately if you use nightwing outside the US;
          interfering with coastal radar systems can be a career-limiting move.
          '';
        };
        networking.nightwing.exitDevice = lib.mkOption {
          type = with types; nullOr str;
          default = null;
          description = ''
            When set, the specified netdev will be used to provide internet access to other
            clients on the mesh.

            Providing a non-null value places the BATMAN device into "server" gateway mode,
            informing mesh peers that this system is sharing its internet connection. It
            also sets up an nftables masquerade rule for outbound connections.

            If unset, this system will not share its internet connection.
            '';
        };
        networking.nightwing.uploadMbps = lib.mkOption {
          type = with types; nullOr int;
          default = null;
          description = ''
            When set, advertise this upload rate (in megabits per second, base 1000) to clients
            looking for a connection. You should specify the typical available upload bandwidth
            on `networking.nightwing.exitDevice`.

            If `networking.nightwing.exitDevice` is not set, this option has no effect.

            If unset, BATMAN's defaults are used.
          '';
        };
        networking.nightwing.downloadMbps = lib.mkOption {
          type = with types; nullOr int;
          default = null;
          description = ''
            When set, advertise this download rate (in megabits per second, base 1000) to clients
            looking for a connection. You should specify the typical available download bandwidth
            on `networking.nightwing.exitDevice`.

            If `networking.nightwing.exitDevice` is not set, this option has no effect.

            If unset, BATMAN's defaults are used.
          '';
        };
        networking.nightwing.networks = lib.mkOption {
          description = lib.mdDoc ''
          All mesh networks to create. The names of each BATMAN network device are
          given by the attribute names you specify.
          '';
          type = attrsOf (submodule {
            options = {
              addressAndMask = lib.mkOption {
                type = types.str;
                description = lib.mdDoc ''
                Static IPv4 address and subnet mask to assign to the mesh interface.
                '';
              };
              routeToExits = lib.mkOption {
                type = types.bool;
                default = false;
                description = ''
                When set to true, the system will try to connect to the internet through the mesh.
                This requires that at least one reachable node in the mesh is running in gateway
                mode and sharing its connection.
                '';
              };
              aqm = {
                enable = lib.mkOption {
                  description = lib.mdDoc ''
                  Enables a [`tc-cake`](https://www.man7.org/linux/man-pages/man8/tc-cake.8.html) qdisc
                  on the BATMAN netdev. Among other things, this can dramatically improve the
                  network's bufferbloat characteristics (ie, it will stay responsive even when
                  operating at high throughput).

                  The defaults are intended for a typical mesh LAN that can push a quarter gigabit between
                  nodes, but shouldn't be a liability in the general case.
                  '';
                  type = types.bool;
                  default = true;
                };
                roundTripMillisec = lib.mkOption {
                  description = lib.mdDoc ''
                  Rough estimated typical-case round-trip time, in milliseconds, for connections on the mesh.
                  Defaults to 20ms.
                  '';
                  type = positiveInt;
                  default = 20;
                };
                bandwidthMbps = lib.mkOption {
                  description = lib.mdDoc ''
                  Rough estimated best-case bandwidth across a mesh link, in Mbps.
                  If unsure, guess high -- setting this too low can throttle bandwidth.

                  Defaults to 250, based on estimates from iperf2 between two line-of-sight Panda
                  PAU0Ds.
                  '';
                  type = positiveInt;
                  default = 250;
                };
              };
              devices = lib.mkOption {
                description = lib.mdDoc ''
                Each BATMAN mesh needs one or more dedicated WiFi devices to run on top of.
                Configure each here, making sure that each network interface name matches the
                attribute name.
                '';
                type = attrsOf (submodule {
                  options = {
                    channel = lib.mkOption {
                      type = types.int;
                      description = lib.mdDoc ''
                      Frequency, in MHz, on which the wireless device should operate.
                      To determine which your hardware supports, run `iw phy`.

                      It's recommended to avoid frequencies that `iw phy` marks "no IR"
                      because it will prevent creating a mesh-point when no established ones are
                      reachable.
                      '';
                      };
                      meshID = lib.mkOption {
                        type = types.str;
                        description = lib.mdDoc ''
                        The mesh ID to join or create. This is basically an ESSID.
                        '';
                      };
                      password = lib.mkOption {
                        type = types.str;
                        description = lib.mdDoc ''
                        The password to use for the mesh network.
                        '';
                      };
                    };
                });
              };
            };
          });
        };
      };

      config = lib.mkIf cfg.enable (
        let
          inherit (lib.attrsets) mapAttrs' mergeAttrsList;
          inherit (lib) lists;
          batmanDevices = attrNames cfg.networks;
          wifiDevices = lists.concatMap (batdev: attrNames cfg.networks.${batdev}.devices) batmanDevices;
          wirelessTxQueueLen = 32;

          defineLinksFor = batdev:
            let
              # XXX: systemd's default link config, 99-default.link, will match if we
              # don't make ours alphabetically earlier. Stick a "10-" at the front of any
              # .link file names.
              prioritizeLinkConfig = wifidev: link: {
                name = "10-${wifidev}";
                value = link;
              };
              linkConfigs = (lib.genAttrs (attrNames cfg.networks.${batdev}.devices) (wifidev: {
                matchConfig.OriginalName = wifidev;
                linkConfig = {
                  # WiFi devs have beefy internal buffers (the Panda PAU0D buffers about 1000 packets at our
                  # MTU, going by the mean ping time elevation in a maximum throughput state).
                  # Don't exacerbate bufferbloat by adding Linux's giant default buffer size.
                  # On a fresh boot, we wouldn't expect this to be a problem (this device should
                  # use the "noqueue" qdisc), but we're being paranoid in case a qdisc that cares
                  # about this were added externally.
                  TransmitQueueLength = wirelessTxQueueLen;
                  NamePolicy = "keep kernel database onboard slot path";
                  AlternativeNamesPolicy = "database onboard slot path";
                  # Wouldn't recommend changing this: BATMAN gateway announcements linger for a long
                  # time. If you're sharing your internet connection and one of your BATMAN WiFi devices
                  # craps out momentarily, re-joining the mesh with the same MAC allows peers using
                  # your internet connection to recover more quickly. Otherwise, they'd have to wait on
                  # BATMAN's throughput estimation to penalize your previous MAC enough to push them
                  # onto an alternative gateway.
                  MACAddressPolicy = "persistent";
                };
              })) // {
                ${batdev} = {
                  matchConfig.OriginalName = batdev;
                  # pretty sure CAKE doesn't give a damn about this, but the user might disable AQM
                  linkConfig.TransmitQueueLength = wirelessTxQueueLen;
                };
              };
            in
              mapAttrs' prioritizeLinkConfig linkConfigs;
          defineNetworksFor = batdev:
            (lib.genAttrs (attrNames cfg.networks.${batdev}.devices) (wifidev: {
              name = wifidev;
              linkConfig = {
                MTUBytes = "1532";
                ActivationPolicy = "always-up";
                RequiredForOnline = "no";
                ARP = "no";
              };
              networkConfig = {
                BatmanAdvanced = batdev;
                LinkLocalAddressing = "no";
                IgnoreCarrierLoss = "yes";
                LLMNR = "no";
                DHCP = "no";
              };
            })) // {
              ${batdev} = {
                name = batdev;
                address = [ cfg.networks.${batdev}.addressAndMask ];
                linkConfig = {
                  MTUBytes = "1500";
                  ActivationPolicy = "always-up";
                  RequiredForOnline = "no";
                };
                networkConfig = {
                  MulticastDNS = "yes";
                  IgnoreCarrierLoss = "yes";
                  IPMasquerade = lib.mkIf (cfg.exitDevice != null) "ipv4";
                  IPv4ProxyARP = true;
                  LLMNR = "no";
                  DHCP = "no";
                  # XXX:
                  # LL addresses are tempting for automatic ipv6 assignments,
                  # but they're kind of unwieldy (eg, trying to ping somehost.local
                  # will probably give you an ipv6 address, but you may need to specify
                  # a scope id or netdev to ping that address :/)
                  LinkLocalAddressing = "no";
                };
                routes = lib.mkIf cfg.networks.${batdev}.routeToExits [{
                  routeConfig = {
                    Source = "0.0.0.0";
                    Destination = "0.0.0.0/0";
                    Metric = 666;
                  };
                 }];
                extraConfig = with cfg.networks.${batdev}.aqm; lib.mkIf enable ''
                [CAKE]
                RTTSec = ${toString roundTripMillisec}ms
                Bandwidth = ${toString bandwidthMbps}M
                # BATMAN header adds 32 bytes/packet
                OverheadBytes = 32
                PriorityQueueingPreset = diffserv4
                FlowIsolationMode = dst-host
                '';
              };
            };
        in {
          assertions = lib.mkIf cfg.enable [
            {
              assertion = lists.allUnique wifiDevices;
              message = "wifi devs in networking.nightwing.networks.{...}.devices must appear at most once across all networks";
            }
            {
              assertion = (lists.intersectLists batmanDevices wifiDevices) == [];
              message =
                ''
                BATMAN mesh netdevs cannot be used in place of "plain" WiFi devices.
                Don't put a BATMAN device in networking.nightwing.networks.{...}.devices, and
                don't use a WiFi device name as networking.nightwing.networks.$name.
                '';
            }
            {
              assertion = (cfg.exitDevice != null) -> !(builtins.elem cfg.exitDevice wifiDevices);
              message = "Exit netdev ${cfg.exitDevice} can't also be enslaved to any BATMAN mesh netdev";
            }
            {
              assertion = (cfg.exitDevice != null) -> !(builtins.elem cfg.exitDevice batmanDevices);
              message = "A nightwing-managed BATMAN device cannot be used as an exit device";
            }
          ];
          networking.nat = lib.mkIf (cfg.exitDevice != null) {
            enable = true;
            externalInterface = cfg.exitDevice;
            internalInterfaces = batmanDevices;
          };
          networking.nftables = lib.mkIf (cfg.exitDevice != null) {
            enable = true;
          };
          networking.firewall.extraForwardRules = lib.mkIf (cfg.exitDevice != null) (
            let forwardFrom = batdev: "iifname ${batdev} oifname ${cfg.exitDevice} accept";
            in lib.strings.concatStringsSep "\n" (map forwardFrom batmanDevices)
          );

          # Need a kernel that supports 802.11s -- there's a PR for this, but
          # fix it with a little patch in the meantime:
          # https://github.com/NixOS/nixpkgs/pull/291062
          boot.kernelPatches = with lib.kernel; lib.optionals ((config.boot.kernelPackages.commonStructuredConfig.MAC80211_MESH.tristate or no.tristate) == yes.tristate) {
            name = "enable-mac80211-mesh";
            patch = null;
            extraStructuredConfig.MAC80211_MESH = yes;
          };
          # Similar issue w/ wpa_supplicant; see comment on this flake's default overlay
          nixpkgs.overlays = [self.overlays.default];

          # Ensure that NetworkManager doesn't try to reconfigure any WiFi devs participating in
          # any of our mesh nets
          networking.networkmanager.unmanaged =
            let wifiDevices = lib.concatMap (net: attrNames net.devices) (attrValues cfg.networks);
            in map (x: "interface-name:${x}") wifiDevices;

          # Use systemd-networkd to manage address assignment, define batman devs
          systemd.network = {
            enable = true;
            wait-online.enable = false;
            netdevs = lib.genAttrs batmanDevices (batmanDev: {
              netdevConfig = {
                Name = batmanDev;
                Kind = "batadv";
              };
              batmanAdvancedConfig = {
                GatewayMode =
                    if cfg.exitDevice != null                 then "server" else
                    if cfg.networks.${batmanDev}.routeToExits then "client" else "off";
                RoutingAlgorithm = "batman-v";
                OriginatorIntervalSec = "5";
              };
              # these settings not present in the nixos module; have to bring along the snippet
              # ourselves
              extraConfig =
                let
                  configureBandwidth = inAttr: outAttr:
                    lib.optionalString
                      ((cfg.${inAttr} != null) && (cfg.exitDevice != null))
                      "${outAttr} = ${toString cfg.${inAttr}}M";
                in
                  ''
                  [BatmanAdvanced]
                  ${configureBandwidth "uploadMbps" "GatewayBandwidthUp"}
                  ${configureBandwidth "downloadMbps" "GatewayBandwidthDown"}
                  '';
            });
            networks = mergeAttrsList (map defineNetworksFor batmanDevices);
            links = mergeAttrsList (map defineLinksFor batmanDevices);
          };

          # Configure all WiFi devs with wpa_supplicant
          networking.supplicant =
            let wifiDevs = mergeAttrsList (map (bat: cfg.networks.${bat}.devices) batmanDevices);
                wifiDevNames = attrNames wifiDevs;
            in lib.genAttrs wifiDevNames (name: {
              configFile.path = "/dev/null"; # ugh, really?
              # documented at https://w1.fi/cgit/hostap/plain/wpa_supplicant/wpa_supplicant.conf
              extraConf = ''
              country=${cfg.country}
              p2p_disabled=1
              # Force hash-to-element for SAE password element derivation;
              # there are widely-known sidechannel attacks on the older hunt-and-peck method
              sae_pwe=1
              pmf=1 # protected mgmt frames
              mesh_max_inactivity=10 # BATMAN ELP traffic should make 10s acceptable
              user_mpm=1 # don't rely on driver mesh peering manager

              network={
                ieee80211w=2 # force protected management frames
                ssid="${wifiDevs.${name}.meshID}"
                sae_password="${wifiDevs.${name}.password}"
                key_mgmt=SAE
                # Forbid "transition mode" support (we don't need to support WPA2 clients)
                sae_pk=1
                mesh_fwding=0 # disables default 802.11s routing algo
                frequency=${toString wifiDevs.${name}.channel}
                mode=5 # mesh-point
              }
            '';
            });
        });
      };
  };
}
